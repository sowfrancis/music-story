class StoriesController < ApplicationController
	before_action :authenticate_user!, only: [:new, :create]
	before_action :find_story, only: [:show, :edit, :update]

	def new
		@story = Story.new
	end

	def create
		@story = current_user.stories.new(story_params)

		if @story.save
			redirect_to story_path(@story)
		else
			render "new"
		end
	end

	def show ;end

	def edit ;end

	def update
		byebug

		if @story.update(story_params)
			redirect_to story_path(@story)
		else
			render :edit
		end
	end

	private

	def find_story
		@story = Story.find(params[:id])
	end

	def story_params
		params.require(:story).permit(:title, :description, :user_id, :images, :videos)
	end
end
