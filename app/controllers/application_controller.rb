class ApplicationController < ActionController::Base
	before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
    stored_location_for(resource) || users_path
  end

  def after_update_path_for(resource)
    stored_location_for(resource) || user_path(current_user)
  end

	protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:last_name, :first_name, 
    :nickname, :latitude, :longitude, :avatar, :artist, :address])
    devise_parameter_sanitizer.permit(:account_update, keys: [:last_name, :first_name, 
    :nickname, :latitude, :longitude, :avatar, :artist, :address, :city])
  end

end
