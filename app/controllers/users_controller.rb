class UsersController < ApplicationController
	include ActiveStorage::SetCurrent
	before_action :find_user, only: [:show]

	def index
		@users = User.artists
	end

	def show; end

	private

	def find_user
		@user = User.includes(:stories).find(params[:id])
	end

	def user_params
		params.require(:user).permit(:last_name, :first_name, :nickname, :latitude,
    :longitude, :avatar, :artist, :address)
	end
end
