module StoriesHelper

	def form_story_url(story)
		if story.new_record?
			stories_path
		else
			story_path(story)
		end
	end

	def story_title_form(story)
		if story.new_record?
			content_tag(:div, class: 'title mb-3') do
				content_tag(:h2, 'Nouvelle histoire')
			end
		else
			content_tag(:div, class: 'title mb-3') do
				content_tag(:h2, 'Edite ton histoire')
			end			
		end
	end
end
