# frozen_string_literal: true

require 'action_text'

class NewEditStoryFormComponent < ViewComponent::Base
  include StoriesHelper
  include ActionText::Engine.helpers

  def initialize(story:)
    @story = story
  end

  private

  def main_app
    Rails.application.class.routes.url_helpers
  end
end
