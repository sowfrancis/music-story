# frozen_string_literal: true

class NavbarComponent < ViewComponent::Base
  include Devise::Controllers::Helpers
  include UsersHelper
  
  def initialize(navbar:)
    @navbar = navbar
  end

end
