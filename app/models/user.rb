class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one_attached :avatar
  has_many :stories

  validates :nickname, :address, presence: true

  geocoded_by :address
  after_validation :geocode

  reverse_geocoded_by :latitude, :longitude
  after_validation :reverse_geocode

  after_create :update_city

  scope :artists, -> { where(artist: true) }

  def update_city
    town = Geocoder.search address

    update(city: town[0].data['address']['city']) unless town.nil?
  end
end
