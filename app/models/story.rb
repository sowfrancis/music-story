class Story < ApplicationRecord
  belongs_to :user
  has_many_attached :images
  has_many_attached :videos
  has_rich_text :description

  validates :title, :description, presence: true
end
