Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  root to: "users#index"

  devise_for :users
  resources :stories, only: [:new, :create, :show, :edit, :update]
  resources :users, only: [:index, :show]
end
