# Pin npm packages by running ./bin/importmap

pin "application", preload: true
pin "jquery", to: "jquery.min.js", preload: true
pin "bootstrap", to: "bootstrap.min.js", preload: true
pin "popper.js", to: "popper.min.js"
pin "trix"
pin "@rails/actiontext", to: "actiontext.js"
