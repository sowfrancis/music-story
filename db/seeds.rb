# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# Create an 100 users with at list 10 stories

addresses = [
	'Paris',
	'London',
	'Berlin',
	'Bangui',
	'Hong kong'
]


puts '#Prepare to create 40 users'

40.times do
	Fabricate(:user, address: addresses.sample)
end

puts '#Users have been created'

puts '#Prepare to create Stories for each users'

users = User.all.each do |user|
	Fabricate.times(10, :story, title: FFaker::Book.title,
	 description: FFaker::Book.description, user_id: user.id)
end

puts '#Stories have been created'

Story.all.each do |story|
	ActionText::RichText.create!(record_type: 'Story', record_id: story.id, 
		name: 'content', body: FFaker::Book.description)
end

puts '#Stories description have been injected'
