class AddAttributesToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :last_name, :string
    add_column :users, :first_name, :string
    add_column :users, :nickname, :string
    add_column :users, :latitude, :decimal, :precision =>  10, :scale => 6 
    add_column :users, :longitude, :decimal, :precision => 10, :scale => 6 
    add_column :users, :artist, :boolean
  end
end
