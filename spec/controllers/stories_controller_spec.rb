# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StoriesController, type: :controller do
  describe 'GET /index' do
    it 'should get all the stories' do
      user = Fabricate(:user, address: 'Paris')
      story1 = Fabricate(:story, title: 'Crazy', description: 'description', user: user)
      story2 = Fabricate(:story, title: 'Unknown', description: 'description2', user: user)

      get :index

      expect(assigns(:stories)).to eq [story1, story2]
    end
  end

  describe 'POST /create' do
    context 'success' do
      it 'should create a story' do
        user = Fabricate(:user, address: 'Japon')

        sign_in user

        expect(Story.count).to eq 0

        post :create, params: { story: { title: 'Crazy', description: 'text', user_id: user.id } }

        expect(Story.count).to eq 1
        expect(response).to redirect_to stories_path
      end
    end

    context 'failure' do
      it 'should not create a story' do
        user = Fabricate(:user, address: 'berlin')

        sign_in user

        expect(Story.count).to eq 0

        post :create, params: { story: { title: '', description: 'text', user_id: user.id } }

        expect(Story.count).to eq 0
      end
    end
  end

  describe 'GET /show' do
    it 'should get a story' do
      user = Fabricate(:user, address: 'berlin')
      story = Fabricate(:story, user_id: user.id)

      get :show, params: { id: story.id }

      expect(response.should).to render_template :show
      expect(response.status).to eq 200
    end
  end

  describe 'Get /update' do
    context 'success' do
      it 'should update a story' do
        user = Fabricate(:user, address: 'Paris')
        story = Fabricate(:story, user_id: user.id, title: 'Example')

        get :update, params: { id: story.id, story: { title: 'New Example' } }

        story.reload

        expect(story.title).to eq 'New Example'
        expect(response.status).to eq 302
        expect(response).to redirect_to story_path(story)
      end
    end

    context 'failure' do
      it 'should not update a story' do
        user = Fabricate(:user, address: 'Paris')
        story = Fabricate(:story, user_id: user.id, title: 'Example')

        get :update, params: { id: story.id, story: { title: '' } }

        story.reload

        expect(response.status).to eq 200
        expect(response).to render_template :edit
      end
    end
  end
end
