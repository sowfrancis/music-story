# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe 'GET index' do
    it 'should get all users' do
      user = Fabricate(:user, address: 'Paris')
      user2 = Fabricate(:user, address: 'Berlin')

      get :index

      expect(assigns(:users)).to eq [user, user2]
      expect(response.status).to eq 200
    end
  end

  describe 'GET show' do
    it 'should get a user' do
      user = Fabricate(:user, address: 'Paris')

      get :show, params: { id: user.id }

      expect(response.should).to render_template :show
      expect(response.status).to eq 200
    end
  end
end
