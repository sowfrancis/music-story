require 'rails_helper'

RSpec.describe User, type: :model do

  context 'Validations' do
    it { should validate_presence_of :nickname }
    it { should validate_presence_of :address }
  end

  context 'Associations' do
    it { should have_many :stories }
    it { should have_one_attached :avatar }
  end

  context 'Callback' do
    let(:user) { Fabricate.build(:user, address: 'Paris') }
    
    context 'Geocoded_by Callback' do
      it 'should call the geocoded user address and update city attributes' do
        address = "Charlemagne et ses leudes, Parvis Notre-Dame - Place Jean-Paul II, Quartier Les Îles, 4th Arrondissement, Paris, Ile-de-France, Metropolitan France, 75004, France"

        expect(user.address).to eq "Paris"

        user.save!

        expect(user.address).to eq address
      end
    end

    context 'Reverse_geocoded_by Callback' do
      it 'should update lat and lon' do
        expect(user.latitude).to eq nil
        expect(user.longitude).to eq nil

        user.save!

        expect(user.latitude).to eq "48.853543".to_f
        expect(user.longitude).to eq "2.348198".to_f
      end
    end

    context 'Update city attribute' do
      it 'should update city' do
        expect(user.city).to eq nil

        user.save!

        expect(user.city).to eq 'Paris'        
      end
    end
  end
end
