require 'rails_helper'

RSpec.describe Story, type: :model do

  context 'Validations' do
    it { should validate_presence_of :title }
    it { should validate_presence_of :description }
  end

  context 'Associations' do
    it { should belong_to :user }
    it { should have_many_attached :images }
    it { should have_many_attached :videos }
  end
end
