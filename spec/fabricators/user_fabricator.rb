Fabricator(:user) do
  email { FFaker::Internet.email }
  password FFaker::Internet.password
  last_name { FFaker::Name.last_name }
  first_name { FFaker::Name.first_name }
  nickname { FFaker::Name.last_name }
  address FFaker::Address.city
  latitude nil
  longitude nil
  artist true
  avatar { Rack::Test::UploadedFile.new(Rails.root.join('app/assets/images/francis.jpeg'), 'image/jpeg') }
end
