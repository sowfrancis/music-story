Fabricator(:story) do
  user_id nil
  title { FFaker::Book.title }
  description { FFaker::Book.description  }
end