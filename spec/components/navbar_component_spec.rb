# frozen_string_literal: true

require "rails_helper"

RSpec.describe NavbarComponent, type: :component do

  it "render the navbar" do
    expect(render_inline(described_class.new(navbar: 'Navbar')) { 'Navbar'})

    expect(page).to have_css 'navbar navbar-expand-lg navbar-light bg-light'
  end
end
