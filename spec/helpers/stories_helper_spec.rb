require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the StoriesHelper. For example:
#
# describe StoriesHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe StoriesHelper, type: :helper do
  
  describe "form_story_url" do
    let(:user) { Fabricate(:user, address: 'Paris') }
    let(:story) { Fabricate(:story, user: user) }
    let(:new_story) { Fabricate.build(:story) }

    context "new record" do
      it 'should return the correct url form story' do
        expect(helper.form_story_url(new_story)).to eq "/stories"
      end
    end

    context "existing recor" do
      it 'should return the correct url form story' do
        expect(helper.form_story_url(story)).to eq "/stories/#{story.id}"
      end
    end
  end

  describe "story_title_form" do
    let(:user) { Fabricate(:user, address: 'Paris') }
    let(:story) { Fabricate(:story, user: user) }
    let(:new_story) { Fabricate.build(:story) }

    context "new record" do
      it 'should render the right title' do
        expect(helper.story_title_form(new_story)).to eq "<div class=\"title mb-3\"><h2>Nouvelle histoire</h2></div>"
      end
    end      

    context "existing record" do
      it 'should render the right title' do
        expect(helper.story_title_form(story)).to eq "<div class=\"title mb-3\"><h2>Edite ton histoire</h2></div>"
      end
    end      
  end
end
